package com.hackathon.trading.rest;

import java.util.List;

import com.hackathon.trading.model.Trade;
import com.hackathon.trading.service.TradeService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping("v1/trade")
public class TradeController {

    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);

    @Autowired
    private TradeService tradeService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Trade> retriveAllTrades() {
        LOG.debug("Retrieve Trades Request Received.");
        return tradeService.retrieveTrades();
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Trade> requestTrades(@RequestBody Trade trade) {
        LOG.debug("Retrieve Trades Request Received.");
        return new ResponseEntity<Trade>(tradeService.requestTrade(trade), HttpStatus.CREATED);
    }

}
