package com.hackathon.trading.service;

import java.util.List;

import com.hackathon.trading.dao.TradeMongoRepo;
import com.hackathon.trading.model.Trade;
import com.hackathon.trading.model.TradeState;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TradeService {

    @Autowired
    private TradeMongoRepo tradeMongoRepo;


    /**
     * Retrieve all trades
     * @param id
     * @param state
     */
    public List<Trade> retrieveTrades() {
        return tradeMongoRepo.findAll();
    }

    /**
     * Create/Insert trade
     * @param trade
     * @return
     */
    public Trade requestTrade(Trade trade) {
        return tradeMongoRepo.save(trade);
    }

    /**
     * Delete trade if not CREATED, FILLED, PENDING OR CANCELLED
     * @param id
     * @param state
     */
    public void deleteTrade(String id, TradeState state) {

        if(state.equals(TradeState.CREATED) || state.equals(TradeState.PROCESSING) || state.equals(TradeState.REJECTED)){

            

        } else {

            tradeMongoRepo.deleteById(id);
        }

    }

    

}
